<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:url value="/" var="raiz" />
<c:url value="/usuario/autenticar" var="urlAutenticarUsuario" />
<!DOCTYPE html>
<html>
<head>

	<c:import url="templates/head.jsp"/>
	<c:import url="templates/header.jsp"/>
	
</head>
<body>

<div class="container ">
<form class="form-horizontal" action="${urlAutenticarUsuario}" method="post">
  <div class="form-group">
    <label class="control-label col-sm-2" for="email">E-mail:</label>
    <div class="col-sm-10">
     <input class="form-control" name="email" type="email" required="required" maxlength="120" id="inputEmail" />
     
     
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="pwd">Senha:</label>
    <div class="col-sm-10"> 
      <input class="form-control" name="senha" type="password" required="required" maxlength="20" />
    </div>
  </div>

  <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit"  class="btn btn-secondary center">Entrar</button>
    </div>
  </div>
</form>

</div>
		<!-- div>
			<form action="${urlAutenticarUsuario}" method="post">
				<label>
					E-mail
					<input name="email" type="email" required="required" maxlength="120" id="inputEmail" />
				</label>
				<label>
					Senha
					<input name="senha" type="password" required="required" maxlength="20" />
				</label>
				<button class="btn" type="submit">ENTRAR</button>
			</form>
		</div-->	
</body>
</html>