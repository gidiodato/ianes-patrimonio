<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url value="/app/adm/usuario/editar" var="urlEditarUsuario" />
<c:url value="/app/adm/" var="urlIndex" />
<c:url value="/app/adm/usuario/novo" var="urlNovoUsuario" />
<c:url value="/app/adm/usuario/deletar" var="urlDeletarUsuario" />

<!DOCTYPE html>
<html>
<head>
<c:import url="../../templates/head.jsp" />
</head>
<body>

	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="${urlIndex}">Página
					principal</a></li>
			<li class="breadcrumb-item"><a href="${urlNovoUsuario}">Cadastrar
					novo usuário</a></li>
			<li class="breadcrumb-item active" aria-current="page">Lista de
				usuários</li>
		</ol>
	</nav>

	<c:import url="../../templates/header.jsp" />
	
	<div class="center">
	
	<h1>Lista de Usuários</h1>
	</div>
	<div class="container">
		<table style="margin-top: 40px" class="table table-hover">
			<thead>
				<tr>
					<th>Nome</th>
					<th>Sobrenome</th>
					<th colspan="2">Ações</th>

				</tr>
			</thead>
			<tbody>
				<c:forEach items="${usuarios}" var="usuario">
					<tr>
						<td>${usuario.nome}</td>
					<td>${usuario.sobrenome}</td>
					<td><a href="${urlDeletarUsuario}?id=${usuario.id}">Deletar</a></td>
					<td><a href="${urlEditarUsuario}?id=${usuario.id}">Alterar</a></td>
						
						
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>







	<!-- h1>Lista de Usuários</h1>

	<table>
		<thead>

			<tr>

				<th>Nome</th>
				<th>Sobrenome</th>
				<th>Ações</th>

			</tr>

		</thead>

		<tbody>

			<%-- Gerar o conteúdo para listar as categorias --%>

			<c:forEach items="${usuarios}" var="usuario">

				<tr>


					<td>${usuario.nome}</td>
					<td>${usuario.sobrenome}</td>
					<td><a href="${urlDeletarUsuario}?id=${usuario.id}">Deletar</a></td>
					<td><a href="${urlEditarUsuario}?id=${usuario.id}">Alterar</a></td>

				</tr>

			</c:forEach>

		</tbody>

	</table-->
</body>
</html>