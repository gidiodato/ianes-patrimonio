<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:url value="/" var="raiz" />
<c:url value="/app/adm/listaUsuarios" var="urlListaUsuariosAdm" />
<c:url value="/app/adm/listaPatrimonios" var="urlListaPatrimoniosAdm" />
<c:url value="/app/adm/listaAmbientes" var="urlListaAmbientesAdm" />
<c:url value="/app/adm/listaItensPatrimonio" var="urlListaItensPatrimonioAdm" />
<c:url value="/sair" var="sair" />

<!DOCTYPE html>
<html>
<head>
<c:import url="../../templates/head.jsp"/>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Ianes</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="${urlListaUsuariosAdm}">Listar usuários<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="${urlListaPatrimoniosAdm}">Listar patrimônios</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="${urlListaAmbientesAdm}">Listar ambientes</a>
      </li>
	<li class="nav-item">
        <a class="nav-link" href="${urlListaItensPatrimonioAdm}">Listar itens de patrimônio</a>
      </li>
    </ul>
  </div>
</nav>

<c:import url="../../templates/header.jsp"/>

<div class="center">
	<form style="display: inline" action="${sair}" method="get">
  <button class="btn btn-secondary">Sair</button>
</form>
</div>





	<!-- a href="${urlListaUsuariosAdm}">Listar usuários</a><br>
	<a href="${urlListaPatrimoniosAdm}">Listar patrimônios</a><br>
	<a href="${urlListaAmbientesAdm}">Listar ambientes</a><br-->
</body>
</html>