<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:url value="/app/adm/usuario/deletar" var="urlUsuarioDeletar" />
<c:url value="/app/adm/usuario/salvar" var="urlSalvarUsuario" />
<c:url value="/app/adm/" var="urlIndex" />
<c:url value="/app/adm/listaUsuarios" var="urlListaUsuarios" />

<!DOCTYPE html>
<html>
<head>
<c:import url="../../templates/head.jsp" />
</head>
<body>
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="${urlIndex}">Página
					principal</a></li>
			<li class="breadcrumb-item"><a href="${urlListaUsuarios}">Lista
					de usuários</a></li>
			<li class="breadcrumb-item active" aria-current="page">Cadastro de
				usuários</li>
		</ol>
	</nav>
	<c:import url="../../templates/header.jsp" />
	<h1>Cadastro de Usuário</h1>


	<form:form modelAttribute="usuario" action="${urlSalvarUsuario}"
		method="post" class="labels-d-block">

		<form:hidden path="id" />

		<label class="control-label col-sm-2" for="inputNome">NOME</label>
		<form:input path="nome" id="inputNome" />
		<form:errors path="nome" element="div" cssClass="error" />
		<br>

		<label class="control-label col-sm-2" for="inputSobrenome">SOBRENOME</label>
		<form:input path="sobrenome" id="inputSobrenome" />
		<form:errors path="sobrenome" element="div" cssClass="error" />
		<br>

		<%--Campos exclusivos para cadastro --%>
		<c:if test="${empty usuario.id}">

			<label class="control-label col-sm-2" for="inputEmail">EMAIL</label>
			<form:input path="email" type="email" id="inputEmail" />
			<form:errors path="email" element="div" cssClass="error" />
			<br>


			<label class="control-label col-sm-2" for="inputSenha">SENHA</label>
			<form:password path="senha" id="inputSenha" />
			<form:errors path="senha" element="div" cssClass="error" />
			<br>

			<label class="control-label col-sm-2" for="inputConfirmaSenha">CONFIRMAR
				SENHA</label>
			<input type="password" id="inputConfirmaSenha"
				name="confirmacaoDeSenha" />
			
			<br>

		</c:if>

		<label class="control-label col-sm-2"> Administrador <input
			type="checkbox" name="isAdministrador"
			${usuario.tipo eq 'ADMINISTRADOR' ? 'checked'  : ''}
			id="inputAdministrador">
		</label>
		<br>
		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit">SALVAR</button>
		</div>
		<c:if test="${not empty usuario.id}">
			<a href="${urlUsuarioDeletar}?id=${usuario.id}">DELETAR</a>
		</c:if>
	</form:form>


</body>
</html>