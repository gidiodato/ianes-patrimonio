<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url value="/" var="raiz" />
<c:url value="/app/usuario/alterarSenha" var="urlAlterarSenha" />
<c:url value="/app/listaItensPatrimonio" var="urlListaItemPatrimonio" />
<c:url value="/app/listaPatrimonios" var="urlListaPatrimonios" />
<c:url value="/app/listaAmbiente" var="urlListaAmbiente" />
<c:url value="/sair" var="sair" />

<!DOCTYPE html>
<html>
<head>
<c:import url="../templates/head.jsp" />
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">Ianes</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNav" aria-controls="navbarNav"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
				<li class="nav-item active"><a class="nav-link"
					href="${urlAlterarSenha}">Alterar senha<span class="sr-only">(current)</span></a>
				</li>

				<li class="nav-item"><a class="nav-link"
					href="${urlListaItemPatrimonio}">Listar itens patrimonio</a></li>
				<li class="nav-item"><a class="nav-link"
					href="${urlListaPatrimonios}">Listar patrimonios</a></li>
				<li class="nav-item"><a class="nav-link"
					href="${urlListaAmbiente}">Listar ambientes</a></li>
			</ul>
		</div>
	</nav>

	<c:import url="../templates/header.jsp" />
	<div class="center">
		<form style="display: inline" action="${sair}" method="get">
			<button class="btn btn-secondary">Sair</button>
		</form>
	</div>


	<!-- a href="${urlAlterarSenha}">Alterar senha</a>
	<br>
	<a href="${urlPatrimonioNovo}">Cadastrar item patrimonio</a>
	<br>
	<a href="${urlListaPatrimonios}">Listar patrimonios</a>
	<br>
	<a href="${urlListaAmbiente}">Listar ambientes</a>
	<br-->


</body>
</html>