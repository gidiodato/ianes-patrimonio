<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:url value="/app/adm/ambiente/deletar" var="urlAmbienteDeletar" />
<c:url value="/app/adm/ambiente/salvar" var="urlSalvarAmbiente" />
<c:url value="/app/adm/" var="urlIndex" />
<c:url value="/app/adm/listaAmbientes" var="urlListaAmbientes" />

<!DOCTYPE html>
<html>
<head>
<c:import url="../../templates/head.jsp" />
</head>
<body>

	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="${urlIndex}">Página
					principal</a></li>
			<li class="breadcrumb-item"><a href="${urlListaAmbientes}">Lista
					de ambientes</a></li>
			<li class="breadcrumb-item active" aria-current="page">Cadastro
				de ambientes</li>
		</ol>
	</nav>

	<c:import url="../../templates/header.jsp" />
	<h1>Cadastro de Ambientes</h1>
	<form:form modelAttribute="ambiente" action="${urlSalvarAmbiente}"
		method="post" class="labels-d-block">

		<form:hidden path="id" />

		<label class="control-label col-sm-2" for="inputNome">NOME</label>
		<form:input path="nome" id="inputNome" />
		<form:errors path="nome" element="div" cssClass="error" />
		<br>


		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit">SALVAR</button>
		</div>
		
		
	</form:form>
</body>
</html>