<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url value="/app/adm/ambiente/editar" var="urlEditarAmbiente" />
<c:url value="/app/adm/ambiente/novo" var="urlNovoAmbiente" />
<c:url value="/app/adm/ambiente/deletar" var="urlDeletarAmbiente" />
<c:url value="/app/adm/" var="urlIndex" />

<!DOCTYPE html>
<html>
<head>
<c:import url="../../templates/head.jsp" />
</head>
<body>

	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="${urlIndex}">Página
					principal</a></li>
			<li class="breadcrumb-item"><a href="${urlNovoAmbiente}">Cadastrar
					novo ambiente</a></li>
			<li class="breadcrumb-item active" aria-current="page">Lista de
				ambientes</li>
		</ol>
	</nav>
	<c:import url="../../templates/header.jsp" />


	<div class="center">

		<h1>Lista de Ambientes</h1>
	</div>
	<div class="container">
		<table style="margin-top: 40px" class="table table-hover">
			<thead>
				<tr>
					<th>Nome</th>
					<th colspan="2">Ações</th>

				</tr>
			</thead>
			<tbody>
				<c:forEach items="${ambientes}" var="ambiente">
					<tr>
						<td>${ambiente.nome}</td>
					<td><a href="${urlDeletarAmbiente}?id=${ambiente.id}">Deletar</a></td>
					<td><a href="${urlEditarAmbiente}?id=${ambiente.id}">Alterar</a></td>


					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>






	<!-- h1>Lista de Ambientes</h1>

	<table>
		<thead>

			<tr>

				<th>Nome</th>
				<th>Ações</th>

			</tr>

		</thead>

		<tbody>

			<%-- Gerar o conteúdo para listar as categorias --%>

			<c:forEach items="${ambientes}" var="ambiente">

				<tr>


					<td>${ambiente.nome}</td>
					<td><a href="${urlDeletarAmbiente}?id=${ambiente.id}">Deletar</a></td>
					<td><a href="${urlEditarAmbiente}?id=${ambiente.id}">Alterar</a></td>

				</tr>

			</c:forEach>

		</tbody>

	</table-->
</body>
</html>