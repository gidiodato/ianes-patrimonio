<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url value="/app/" var="urlIndex" />

<!DOCTYPE html>
<html>
<head>
<c:import url="../templates/head.jsp" />
</head>
<body>

	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="${urlIndex}">Página
					principal</a></li>
			<li class="breadcrumb-item active" aria-current="page">Lista de
				ambientes</li>
		</ol>
	</nav>
	<c:import url="../templates/header.jsp" />
	<div class="center">

		<h1>Lista de Ambientes</h1>
	</div>
	
	<div class="container">
		<table style="margin-top: 40px" class="table table-hover">
			<thead>
				<tr>
					<th>Nome</th>

				</tr>
			</thead>
			<tbody>
				<c:forEach items="${ambientes}" var="ambiente">
					<tr>
						<td>${ambiente.nome}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>