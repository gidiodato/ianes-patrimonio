<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:url value="/app/adm/patrimonio/deletar" var="urlPatrimonioDeletar" />
<c:url value="/app/adm/patrimonio/salvar" var="urlSalvarPatrimonio" />
<c:url value="/app/adm/" var="urlIndex" />
<c:url value="/app/adm/listaPatrimonios" var="urlListaPatrimonios" />

<!DOCTYPE html>
<html>
<head>
<c:import url="../../templates/head.jsp"/>
</head>
<body>

<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="${urlIndex}">Página
					principal</a></li>
			<li class="breadcrumb-item"><a href="${urlListaPatrimonios}">Lista
					de patrimônios</a></li>
			<li class="breadcrumb-item active" aria-current="page">Cadastro
				de patrimônios</li>
		</ol>
	</nav>

	<c:import url="../../templates/header.jsp" />
	
<h1>Cadastro de Patrimônio</h1>

<form:form modelAttribute="patrimonio" action="${urlSalvarPatrimonio}"
		method="post" class="labels-d-block">

		<form:hidden path="id" />

		<label class="control-label col-sm-2" for="inputNome">Nome</label>
		<form:input path="nome" id="inputNome" />
		<form:errors path="nome" element="div" cssClass="error" />
		<br>
		
		<label class="control-label col-sm-2" for="inputCategoria">Categoria</label>
		<form:input path="categoria" id="inputCategoria" />
		<form:errors path="categoria" element="div" cssClass="error" />
		<br>

		<div class="col-sm-offset-2 col-sm-10">
			<button class="btn btn-secondary" type="submit">SALVAR</button>
		</div>
		
	</form:form>
</body>
</html>