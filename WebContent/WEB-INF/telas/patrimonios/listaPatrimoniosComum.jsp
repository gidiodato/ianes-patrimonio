<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 
 <c:url value="/app" var="urlIndex" />
 <c:url value="/app/patirmonio/novo" var="urlNovoPatrimonio" />
 
<!DOCTYPE html>
<html>
<head>
<c:import url="../templates/head.jsp"/>
</head>
<body>

	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="${urlIndex}">Página
					principal</a></li>
			<li class="breadcrumb-item"><a href="${urlNovoPatrimonio}">Cadastrar
					novo patrimônio</a></li>
			<li class="breadcrumb-item active" aria-current="page">Lista de patrimônios</li>
		</ol>
	</nav>
<div class="center">
<h1>Lista de Patrimônios</h1>
</div>

<div class="container">
		<table style="margin-top: 40px" class="table table-hover">
			<thead>
				<tr>
					<th>Nome</th>
					<th>Categoria</th>
					<th>Data do Cadastro</th>

				</tr>
			</thead>
			<tbody>
				<c:forEach items="${patrimonios}" var="patrimonio">
					<tr>
						<td>${patrimonio.nome}</td>					
						<td>${patrimonio.categoria}</td>	
						<td>${patrimonio.dtCadastro}</td>	
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	
</body>
</html>