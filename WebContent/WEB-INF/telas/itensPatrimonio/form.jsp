<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:url value="/app" var="urlIndex" />
<c:url value="/app/item/salvar" var="urlSalvarItemPatrimonio" />
<c:url value="/app/listaItensPatrimonio" var="urlListaItemPatrimonio" />

<!DOCTYPE html>
<html>
<head>
<c:import url="../templates/head.jsp" />
</head>
<body>
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="${urlIndex}">Página
					principal</a></li>
			<li class="breadcrumb-item"><a href="${urlListaItemPatrimonio}">Lista
					de itens patrimônio</a></li>
			<li class="breadcrumb-item active" aria-current="page">Cadastro
				de itens patrimônio</li>
		</ol>
	</nav>
	<c:import url="../templates/header.jsp" />
	<form:form modelAttribute="itemPatrimonio"
		action="${urlSalvarItemPatrimonio}" method="post"
		class="labels-d-block">


		<form:hidden path="id" />

		<div class="container">
			<div class="input-group mb-3">
				<div class="input-group-prepend">
					<label class="input-group-text" for="inputGroupSelect01">Patrimônios</label>
				</div>

				<form:select class="custom-select" path="patrimonio.id"
					items="${pesquisasPatrimonio}" itemValue="id" itemLabel="nome"></form:select>
			</div>

			<div class="input-group mb-3">
				<div class="input-group-prepend">
					<label class="input-group-text" for="inputGroupSelect01">Ambientes</label>
				</div>
				<form:select class="custom-select" path="ambiente.id"
					items="${pesquisasAmbientes}" itemValue="id" itemLabel="nome"></form:select>
			</div>
		</div>

		<div class="col-sm-offset-2 col-sm-10 container center">
			<button class="btn btn-secondary" type="submit">Salvar</button>
		</div>

	</form:form>
</body>
</html>