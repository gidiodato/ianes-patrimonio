<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

 <c:url value="/app/adm" var="urlIndex" />
<c:url value="/app/adm/item/deletar" var="urlDeletarItem" />
<c:url value="/app/adm/item/novo" var="urlNovoItemPatrimonio" />
<c:url value="/app/adm/movimentacao/novo" var="urlMovimentarItem" />
<c:url value="/app/adm/listaMovimentacoes" var="urlVerMovimentacoes" />
<!DOCTYPE html>
<html>
<head>
<c:import url="../../templates/head.jsp" />
</head>
<body>
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="${urlIndex}">Página
					principal</a></li>
			<li class="breadcrumb-item"><a href="${urlNovoItemPatrimonio}">Cadastrar
					novo item</a></li>
			<li class="breadcrumb-item active" aria-current="page">Lista de
				itens patrimônio</li>
		</ol>
	</nav>
	<c:import url="../../templates/header.jsp" />

	<div class="center">
		<h1>Lista de Itens Patrimônio</h1>
	</div>
	<div class="container">
		<table style="margin-top: 40px" class="table table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Patrimônio</th>
					<th>Ambiente</th>
					<th colspan="3">Ações</th>

				</tr>
			</thead>
			<tbody>
				<c:forEach items="${itens}" var="item">
					<tr>
						<td>${item.id}</td>
						<td>${item.patrimonio.nome}</td>
						<td>${item.ambiente.nome}</td>
						<td><a href="${urlDeletarItem}?id=${item.id}">Deletar</a></td>
						<td><a href="${urlMovimentarItem}?id=${item.id}">Movimentar</a></td>
						<td><a href="${urlVerMovimentacoes}?id=${item.id}">Ver movimentações</a></td>

					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>