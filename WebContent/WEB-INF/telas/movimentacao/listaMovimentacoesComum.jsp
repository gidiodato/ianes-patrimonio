<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url value="/app/" var="urlIndex" />
<c:url value="/app/listaItensPatrimonio"
	var="urlListaDeItensPatrimonios" />
<!DOCTYPE html>
<html>
<head>
<c:import url="../templates/head.jsp" />
</head>
<body>

	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="${urlIndex}">Página
					principal</a></li>
			<li class="breadcrumb-item"><a
				href="${urlListaDeItensPatrimonios}">Lista de itens patrimônios</a></li>

			<li class="breadcrumb-item active" aria-current="page">Lista de
				movimentações</li>
		</ol>
	</nav>
	<c:import url="../templates/header.jsp" />
	<div class="center">
		<h1>Lista de Movimentações</h1>
	</div>
	<div class="container">
		<table style="margin-top: 40px" class="table table-hover">
			<thead>
				<tr>
					<th>Patrimônio</th>
					<th>Ambiente de Origem</th>
					<th>Ambiente de Destino</th>
					<th>Usuário</th>

				</tr>
			</thead>
			<tbody>
				<c:if test="${empty movimentacoes}">
					<tr>
						<td>Nenhuma movimentação foi realizada nesse item</td>
					</tr>
				</c:if>
				<c:forEach items="${movimentacoes}" var="movimentacao">
					<tr>
						<td>${movimentacao.itemPatrimonio.patrimonio.nome}</td>
						<td>${movimentacao.ambienteOrigem.nome}</td>
						<td>${movimentacao.ambienteDestino.nome}</td>
						<td>${movimentacao.usuario.nome}</td>

					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>