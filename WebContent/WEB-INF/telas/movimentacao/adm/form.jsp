<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:url value="/app/adm" var="urlIndex" />
<c:url value="/app/adm/listaItensPatrimonio"
	var="urlListaItemPatrimonio" />
<c:url value="/app/adm/movimentacao/salvar" var="urlSalvarMovimentacao" />



<!DOCTYPE html>
<html>
<head>
<c:import url="../../templates/head.jsp" />
</head>
<body>
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="${urlIndex}">Página
					principal</a></li>
			<li class="breadcrumb-item"><a href="${urlListaItemPatrimonio}">Lista
					de itens patrimônio</a></li>
			<li class="breadcrumb-item active" aria-current="page">Movimentação
				de itens patrimônio</li>
		</ol>
	</nav>
	<c:import url="../../templates/header.jsp" />
	<!-- p>ID : ${itemPatrimonio.id}</p>
	<p>Patrimônio : ${itemPatrimonio.patrimonio.nome}</p>
	<p>Ambiente em que se encontra : ${itemPatrimonio.ambiente.nome}</p-->
	
	<div class="container">
	<form:form modelAttribute="movimentacao"
		action="${urlSalvarMovimentacao}" method="post" class="labels-d-block">
		
		<form:hidden path="itemPatrimonio.id" value="${itemPatrimonio.id}"/>
		<form:hidden path="ambienteOrigem.id" value="${itemPatrimonio.ambiente.id}"/>
		
		<form:input class="form-control" type="text" path="itemPatrimonio.id" placeholder="ID: ${itemPatrimonio.id}" disabled="true" />
		<br>
		
		<form:input class="form-control" type="text" path="itemPatrimonio.patrimonio.nome" placeholder="Patrimônio: ${itemPatrimonio.patrimonio.nome}" disabled="true" />
		<br>
		
		<form:input class="form-control" type="text" path="itemPatrimonio.patrimonio.categoria" placeholder="Categoria: ${itemPatrimonio.patrimonio.categoria}" disabled="true" />
		<br>
		
		<form:input class="form-control" type="text" path="dtMovimentacao" placeholder="Data da última movimentação: ${ultimaMovimentacao.dtMovimentacao}" disabled="true" />
		<br>
		
		<form:input class="form-control" type="text" path="itemPatrimonio.ambiente.nome" placeholder="Ambiente em que se encontra: ${itemPatrimonio.ambiente.nome}" disabled="true" />
		<br>
		
		
		
		<div class="input-group mb-3">
			<div class="input-group-prepend">
				<label class="input-group-text" for="inputGroupSelect01">Seleciona o novo ambiente</label>
			</div>

			<form:select class="custom-select" path="ambienteDestino.id"
				items="${ambientesPesquisa}" itemValue="id" itemLabel="nome"></form:select>
		</div>
		
		<div class="col-sm-offset-2 col-sm-10 container center">
			<button class="btn btn-secondary" type="submit">Salvar</button>
		</div>

	</form:form>
	</div>


</body>
</html>