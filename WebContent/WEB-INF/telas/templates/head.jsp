<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:url value="/" var="raiz" />
<c:url value="/assets/css" var="cssRaiz" />
<c:url value="/assets/js" var="jsRaiz" />

<title>IANES</title>

<script src="${jsRaiz}/bootstrap.min.js"></script>
<script src="${jsRaiz}/jquery-3.3.1.min.js"></script>
<link rel="stylesheet" href="${cssRaiz}/bootstrap.min.css">
<link rel="stylesheet" href="${cssRaiz}/estilos.css">
<link rel="icon"
	href="https://cdn.icon-icons.com/icons2/259/PNG/128/ic_assignment_turned_in_128_28214.png">