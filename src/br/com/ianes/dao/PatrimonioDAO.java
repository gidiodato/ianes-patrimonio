package br.com.ianes.dao;

import br.com.ianes.models.Patrimonio;

public interface PatrimonioDAO  extends DAO<Patrimonio>{
	
	public Patrimonio buscarPorNome(String nome);
	

}
