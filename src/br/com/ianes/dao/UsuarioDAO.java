package br.com.ianes.dao;

import br.com.ianes.models.Usuario;

public interface UsuarioDAO extends DAO<Usuario>{
	
public Usuario buscarPorEmail(String email);

public Usuario buscarPorEmailESenha(String email, String senha);
}