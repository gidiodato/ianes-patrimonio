package br.com.ianes.dao;

import java.util.Date;
import java.util.List;

import br.com.ianes.models.Movimentacao;

public interface MovimentacaoDAO extends DAO<Movimentacao> {

	public Movimentacao buscarPorDataMovimentacao(Date dtMovimentacao);
	
	public Movimentacao buscarPorItem(Long item);
	
	public List<Movimentacao> buscarPorIdDoItem(Long id);
}
