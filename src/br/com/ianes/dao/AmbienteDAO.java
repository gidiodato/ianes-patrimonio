package br.com.ianes.dao;

import br.com.ianes.models.Ambiente;


public interface AmbienteDAO extends DAO<Ambiente>{

	public Ambiente buscarPorNome(String nome);
}
