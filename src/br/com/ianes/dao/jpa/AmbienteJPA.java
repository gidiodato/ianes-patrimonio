package br.com.ianes.dao.jpa;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.ianes.dao.AmbienteDAO;
import br.com.ianes.models.Ambiente;

@Repository
@Transactional
public class AmbienteJPA implements AmbienteDAO{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void persistir(Ambiente obj) {
		sessionFactory.getCurrentSession().persist(obj);
		
	}

	@Override
	public void alterar(Ambiente obj) {
		sessionFactory.getCurrentSession().update(obj);
		
	}

	@Override
	public void deletar(Ambiente ambiente) {
		sessionFactory.getCurrentSession().delete(ambiente);	
		
	}

	@Override
	public Ambiente buscar(Long id) {
		String hql = "FROM Ambiente u WHERE u.id = :id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("id", id);
		
		//Pegando o primeiro resultado
		List<Ambiente> resultado = query.list();
		
		if(!resultado.isEmpty()) {
			return resultado.get(0);
		}else {
			return null;
		}
	}

	@Override
	public List<Ambiente> buscarTodos() {
		String hql = "FROM Ambiente";
		Query query  = sessionFactory.getCurrentSession().createQuery(hql);
		return query.list();
	}

	@Override
	public Ambiente buscarPorNome(String nome) {
String hql = "FROM Ambiente u WHERE u.nome = :nome";
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		
		query.setParameter("nome", nome);
		
		List<Ambiente> resultado =  query.list();
		
		
		if(!resultado.isEmpty()) {
			return resultado.get(0);
		}else {
			return null;
		}
	}

}
