package br.com.ianes.dao.jpa;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.ianes.dao.PatrimonioDAO;
import br.com.ianes.models.Patrimonio;

@Repository
@Transactional
public class PatrimonioJPA implements PatrimonioDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void persistir(Patrimonio obj) {
		sessionFactory.getCurrentSession().persist(obj);
		
	}

	@Override
	public void alterar(Patrimonio obj) {
		sessionFactory.getCurrentSession().update(obj);	
		
	}

	@Override
	public void deletar(Patrimonio patrimonio) {
		sessionFactory.getCurrentSession().delete(patrimonio);	
		
	}

	@Override
	public Patrimonio buscar(Long id) {
		String hql = "FROM Patrimonio u WHERE u.id = :id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("id", id);
		
		//Pegando o primeiro resultado
		List<Patrimonio> resultado = query.list();
		
		if(!resultado.isEmpty()) {
			return resultado.get(0);
		}else {
			return null;
		}
	}

	@Override
	public List<Patrimonio> buscarTodos() {
		String hql = "FROM Patrimonio";
		Query query  = sessionFactory.getCurrentSession().createQuery(hql);
		return query.list();
	}

	@Override
	public Patrimonio buscarPorNome(String nome) {
String hql = "FROM Patrimonio u WHERE u.nome = :nome";
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		
		query.setParameter("nome", nome);
		
		List<Patrimonio> resultado =  query.list();
		
		
		if(!resultado.isEmpty()) {
			return resultado.get(0);
		}else {
			return null;
		}
	}


}
