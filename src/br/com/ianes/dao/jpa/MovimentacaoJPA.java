package br.com.ianes.dao.jpa;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.ianes.dao.MovimentacaoDAO;
import br.com.ianes.models.ItemPatrimonio;
import br.com.ianes.models.Movimentacao;

@Repository
@Transactional
public class MovimentacaoJPA implements MovimentacaoDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void persistir(Movimentacao obj) {
		sessionFactory.getCurrentSession().persist(obj);

	}

	@Override
	public void alterar(Movimentacao obj) {
		sessionFactory.getCurrentSession().update(obj);

	}

	@Override
	public void deletar(Movimentacao movimentacao) {
		sessionFactory.getCurrentSession().delete(movimentacao);

	}

	@Override
	public Movimentacao buscar(Long id) {
		String hql = "FROM Movimentacao m WHERE m.id = :id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("id", id);

		// Pegando o primeiro resultado
		List<Movimentacao> resultado = query.list();

		if (!resultado.isEmpty()) {
			return resultado.get(0);
		} else {
			return null;
		}

	}

	@Override
	public List<Movimentacao> buscarTodos() {
		String hql = "FROM Movimentacao";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		return query.list();
	}

	@Override
	public Movimentacao buscarPorItem(Long item) {
		String hql = "FROM Movimentacao m WHERE m.itemPatrimonio.id = :item";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("item", item);

		// Pegando o primeiro resultado
		List<Movimentacao> resultado = query.list();

		if (!resultado.isEmpty()) {
			return resultado.get(0);
		} else {
			return null;
		}
	}

	@Override
	public Movimentacao buscarPorDataMovimentacao(Date dtMovimentacao) {
		String hql = "FROM Movimentacao m WHERE m.dtMovimentacao = :dtMovimentacao";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("dtMovimentacao", dtMovimentacao);

		// Pegando o primeiro resultado
		List<Movimentacao> resultado = query.list();

		if (!resultado.isEmpty()) {
			return resultado.get(0);
		} else {
			return null;
		}
	}
	
	@Override
	public List<Movimentacao> buscarPorIdDoItem(Long id) {
		String hql = "FROM Movimentacao m WHERE m.itemPatrimonio.id = :id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("id", id);
		return query.list();
	}

}
