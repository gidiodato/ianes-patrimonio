package br.com.ianes.dao.jpa;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.ianes.dao.ItemDAO;
import br.com.ianes.models.ItemPatrimonio;
import br.com.ianes.models.Patrimonio;

@Repository
@Transactional
public class ItemJPA implements ItemDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void persistir(ItemPatrimonio obj) {
		sessionFactory.getCurrentSession().persist(obj);

	}

	@Override
	public void alterar(ItemPatrimonio obj) {
		sessionFactory.getCurrentSession().update(obj);

	}

	@Override
	public void deletar(ItemPatrimonio itemPatrimonio) {
		sessionFactory.getCurrentSession().delete(itemPatrimonio);

	}

	@Override
	public ItemPatrimonio buscar(Long id) {
		String hql = "FROM ItemPatrimonio u WHERE u.id = :id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("id", id);

		// Pegando o primeiro resultado
		List<ItemPatrimonio> resultado = query.list();

		if (!resultado.isEmpty()) {
			return resultado.get(0);
		} else {
			return null;
		}
	}

	@Override
	public List<ItemPatrimonio> buscarTodos() {
		String hql = "FROM ItemPatrimonio";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		return query.list();
	}

	@Override
	public ItemPatrimonio buscarPorIdPatrimonio(String patrimonio_id) {
		String hql = "FROM ItemPatrimonio u WHERE u.patrimonio_id = :patrimonio_id";

		Query query = sessionFactory.getCurrentSession().createQuery(hql);

		query.setParameter("patrimonio_id", patrimonio_id);

		List<ItemPatrimonio> resultado = query.list();

		if (!resultado.isEmpty()) {
			return resultado.get(0);
		} else {
			return null;
		}
	}

}
