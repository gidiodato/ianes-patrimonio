package br.com.ianes.dao;

import br.com.ianes.models.ItemPatrimonio;

public interface ItemDAO extends DAO<ItemPatrimonio>{
	
	public ItemPatrimonio buscarPorIdPatrimonio(String patrimonio_id);
	
}
