package br.com.ianes.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "movimentacao")
public class Movimentacao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false, unique = false)
	private Date dtMovimentacao;

	@ManyToOne
	@JoinColumn(name = "ambienteOrigem_id", nullable = false)
	private Ambiente ambienteOrigem;

	@ManyToOne
	@JoinColumn(name = "ambienteDestino_id", nullable = false)
	private Ambiente ambienteDestino;

	@ManyToOne
	@JoinColumn(name = "usuario_id", nullable = false)
	private Usuario usuario;

	@ManyToOne
	@JoinColumn(name = "itemPatrimonio_id", nullable = false)
	private ItemPatrimonio itemPatrimonio;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDtMovimentacao() {
		return dtMovimentacao;
	}

	public void setDtMovimentacao(Date dtMovimentacao) {
		this.dtMovimentacao = dtMovimentacao;
	}

	public Ambiente getAmbienteOrigem() {
		return ambienteOrigem;
	}

	public void setAmbienteOrigem(Ambiente ambienteOrigem) {
		this.ambienteOrigem = ambienteOrigem;
	}

	public Ambiente getAmbienteDestino() {
		return ambienteDestino;
	}

	public void setAmbienteDestino(Ambiente ambienteDestino) {
		this.ambienteDestino = ambienteDestino;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public ItemPatrimonio getItemPatrimonio() {
		return itemPatrimonio;
	}

	public void setItemPatrimonio(ItemPatrimonio itemPatrimonio) {
		this.itemPatrimonio = itemPatrimonio;
	}

	@Override
	public String toString() {
		return "Movimentacao [id=" + id + ", dtMovimentacao=" + dtMovimentacao + ", ambienteOrigem=" + ambienteOrigem
				+ ", ambienteDestino=" + ambienteDestino + ", usuario=" + usuario + ", itemPatrimonio=" + itemPatrimonio
				+ "]";
	}

}
