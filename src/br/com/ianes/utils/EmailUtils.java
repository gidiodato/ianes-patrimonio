package br.com.ianes.utils;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailUtils {
	/*
	 * Conta que enviar� o e-mail
	 */
	public static final String remetente = "senai132.info.2017.1s@gmail.com";

	/*
	 * A senha da conta
	 */
	public static final String senhaRemetente = "TecInfoManha2017";

	private static Session getMaiSession() {
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");// servidor de envio de e-mails
		props.put("mail.smtp.socketFactory.port", "465");// Qual � a aporta de servico de envio de e-mail
		props.put("mail.smtp.sockerFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");// Determina que a autenticacao � necessaria
		props.put("mail.smtp.port", "465");// porta de envio de e-mail

		Session session = Session.getInstance(props, new Authenticator() {

			@Override
			protected PasswordAuthentication getPasswordAuthentication() {

				return new PasswordAuthentication(remetente, senhaRemetente);
			}

		});
		return session;
	}

	public static void enviarEmail(String titulo, String corpo, String destinatario)
			throws AddressException, MessagingException {
		Message msg = new MimeMessage(getMaiSession());
		// Definindo destinatario
		msg.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));

		// definindo o remetente
		msg.setFrom(new InternetAddress(remetente));

		// Titulo
		msg.setSubject(titulo);

		// corpo
		msg.setText(corpo);

		// envia o email...
		Transport.send(msg);

	}

}
