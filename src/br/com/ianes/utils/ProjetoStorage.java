package br.com.ianes.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Grava arquivos dentro do pr�prio projeto
 * @author 36315014801
 *
 */
@Component
public class ProjetoStorage {
	
	/**
	 * Guarda informa��es sobre onde esta rodando o projeto
	 */
	
	@Autowired
	private ServletContext context;
	
	/**
	 * Retorna o caminho da asta do projeto
	 * @return
	 */
	public String pegarCaminhoDoProjeto() {
		return context.getRealPath("");
	}
	
	/**Cria um novo arquivo para a foto de perfil do usu�rio em
	 * <pastaDoProjeto>/fotos/<nomeDoArquivo?
	 * @param nomeDoArquivo
	 * @param dadosDoArquivo
	 * @throws IOException 
	 */
	public void armazenarFotoDePerfil(String nomeDoArquivo, byte[] dadosDoArquivo) throws IOException {
		//pega o caminho para a pasta /assets/fotos
		String caminhoFotos = pegarCaminhoDoProjeto() + "assets/fotos";
		String caminhoArquivo = caminhoFotos + "/" + nomeDoArquivo;
		
		//Criar a pasta de fotos caso ela n�o exista
		File pastaFoto = new File(caminhoFotos);
		if(!pastaFoto.exists()) {
			pastaFoto.mkdirs(); //Cria a pasta
		}
		
		//Cria o arquivo na pasta
		File arquivoFoto = new File(caminhoArquivo);
		if(arquivoFoto.exists()) {
			arquivoFoto.delete();
		}
		arquivoFoto.createNewFile();
		
		//Salva os dados do arquivo
		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(arquivoFoto));
		bos.write(dadosDoArquivo);
		bos.close();
	}
}
