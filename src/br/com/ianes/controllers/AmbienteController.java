package br.com.ianes.controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ianes.dao.AmbienteDAO;
import br.com.ianes.models.Ambiente;

@Controller
public class AmbienteController {
	@Autowired
	private AmbienteDAO ambienteDAO;

	@GetMapping("/app/adm/ambiente/novo")
	public String abrirFormNovoAmbiente(Model model) {

		// Passando o objeto que ser� enviado pra tela
		model.addAttribute("ambiente", new Ambiente());

		return "ambiente/adm/form";
	}

	@GetMapping("/app/adm/ambiente/editar")
	public String abrirEdicao(Model model, @RequestParam(name = "id", required = true) Long id,
			HttpServletResponse response) throws IOException {

		model.addAttribute("ambiente", ambienteDAO.buscar(id));

		return "ambiente/adm/form";
	}

	@GetMapping("/app/adm/ambiente/deletar")
	public String deletar(@RequestParam(required = true) Long id, HttpServletResponse response) throws IOException {

		Ambiente ambienteADeletar = ambienteDAO.buscar(id);
		ambienteDAO.deletar(ambienteADeletar);

		return "redirect:/app/adm/listaAmbientes";
	}

	@PostMapping(value = { "/app/adm/ambiente/salvar" })
	public String salvar(@Valid Ambiente ambiente, BindingResult brAmbiente) throws IOException {
		System.out.println(ambiente);

		// Chegando se e-mail j� est� sendo utilizado
		if (ambienteDAO.buscarPorNome(ambiente.getNome()) != null) {
			brAmbiente.addError(new FieldError("ambiente", "nome", "O nome j� esta sendo usado"));
		}

		// Se houverem erros no usu�rio, reabre o formul�rio
		if (brAmbiente.hasErrors()) {
			return "usuario/adm/form";
		} else {
			// Valida��es de altera��o
			if (brAmbiente.hasFieldErrors("nome")) {
				return "usuario/adm/form";
			}
		}

		// Caso cadastro
		if (ambiente.getId() == null) {
			ambienteDAO.persistir(ambiente);
		}
		// Caso altera��o
		else

		{
			// Pega o usu�rio do Hibernate atrav�s do id do form para poder alter�-lo
			Ambiente ambienteBancoDeDados = ambienteDAO.buscar(ambiente.getId());
			ambienteBancoDeDados.setNome(ambiente.getNome());

			ambienteDAO.alterar(ambienteBancoDeDados);
		}

		return "redirect:/app/adm/listaAmbientes";
	}
}
