package br.com.ianes.controllers;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ianes.dao.UsuarioDAO;
import br.com.ianes.models.TiposUsuario;
import br.com.ianes.models.Usuario;
import br.com.ianes.utils.EmailUtils;

@Controller
public class UsuarioController {
	
	@Autowired
	private UsuarioDAO usuarioDAO;
	
	@Autowired
	private ServletContext context;
	
	@GetMapping(value = { "/", "" })
	public String abrirLogin(Model model) {
		System.out.println(context.getRealPath(""));

		return "index";
	}
	
	@GetMapping("/app/adm/usuario/editar")
	public String abrirEdicao(Model model, @RequestParam(name = "id", required = true) Long id,
			HttpServletResponse response) throws IOException {

		model.addAttribute("usuario", usuarioDAO.buscar(id));

		return "usuario/adm/form";
	}
	
	//@GetMapping("/app/adm/usuario")
	//public String abrirLista(Model model) {
	//	model.addAttribute("usuarios", usuarioDAO.buscarTodos());
	//	return "usuario/lista";
	//}
	
	@GetMapping("/app/adm/usuario/novo")
	public String abrirFormNovoUsuario(Model model) {

		// Passando o objeto que ser� enviado pra tela
		model.addAttribute("usuario", new Usuario());

		return "usuario/adm/form";
	}
	
	/*@GetMapping("/app/perfil")
	public String abrirFormEditarUsuarioLogado(Model model) {

		return "usuario/form";
	}*/
	
	@GetMapping("/app/adm/usuario/deletar")
	public String deletar(@RequestParam(required = true) Long id, HttpServletResponse response) throws IOException {

		Usuario usuarioADeletar = usuarioDAO.buscar(id);
		usuarioDAO.deletar(usuarioADeletar);

		return "redirect:/app/adm/listaUsuarios";
	}
	
	@PostMapping(value = { "/app/adm/usuario/salvar" })
	public String salvar(@Valid Usuario usuario, BindingResult brUsuario,
			@RequestParam(name = "confirmacaoDeSenha", required = false) String confirmaSenha,
			@RequestParam(name = "isAdministrador", required = false) Boolean ehAdministrador)
			 throws IOException {
		System.out.println(usuario);
		if (usuario.getId() == null) {
			if (!confirmaSenha.equals(usuario.getSenha())) {
				brUsuario.addError(new FieldError("usuario", "senha", "As senhas n�o s�o iguais"));
			}

			// Chegando se e-mail j� est� sendo utilizado
			if (usuarioDAO.buscarPorEmail(usuario.getEmail()) != null) {
				brUsuario.addError(new FieldError("usuario", "email", "O e-mail j� esta sendo usado"));
			}

			// Se houverem erros no usu�rio, reabre o formul�rio
			if (brUsuario.hasErrors()) {
				return "usuario/adm/form";
			}
		} else {
			// Valida��es de altera��o
			if (brUsuario.hasFieldErrors("nome") || brUsuario.hasFieldErrors("sobrenome")) {
				return "usuario/adm/form";
			}
		}

		// Verificando se o checkbox foi marcado atrav�s da checagem de valor nulo
		System.out.println("� administrador: " + ehAdministrador);
		if (ehAdministrador != null) {
			usuario.setTipo(TiposUsuario.ADMINISTRADOR);
		} else {
			usuario.setTipo(TiposUsuario.COMUM);
		}

		//Caso cadastro
		if (usuario.getId() == null) {
			// Salvando usu�rio hasheando a senha
			usuario.hashearSenha();
			usuarioDAO.persistir(usuario);
			
			String titulo = "Bem-Vindo ao Ianes Patrim�nio";
			String corpo = "Ol�, " + usuario.getNome() + "! Seja bem-vindo ao Ianes Patrim�nio. " +
			"Acesse o link: localhost:8080/ianes-patrimonio/ para realizar o login.";
			
			try {
				EmailUtils.enviarEmail(titulo, corpo, usuario.getEmail());
			} catch (MessagingException e) {
				e.printStackTrace();
			}
		}
		// Caso altera��o
		else

		{
			// Pega o usu�rio do Hibernate atrav�s do id do form para poder alter�-lo
			Usuario usuarioBancoDeDados = usuarioDAO.buscar(usuario.getId());
			usuarioBancoDeDados.setNome(usuario.getNome());
			usuarioBancoDeDados.setSobrenome(usuario.getSobrenome());
			usuarioBancoDeDados.setTipo(usuario.getTipo());

			usuarioDAO.alterar(usuarioBancoDeDados);
		}
		
		return "redirect:/app/adm/listaUsuarios";
	}
	
	@GetMapping({"/app/usuario/alterarSenha"})
	public String alterarSenha(@Valid Usuario usuario, BindingResult brUsuario) {
		
		
		return "usuario/alterarSenha";
	}
	

	
	@PostMapping({"/usuario/autenticar"})
	public String autenticar(@Valid Usuario usuario, BindingResult brUsuario, HttpSession session) {

		usuario.hashearSenha();
		Usuario usuarioBuscado = usuarioDAO.buscarPorEmailESenha(usuario.getEmail(), usuario.getSenha());
		if (usuarioBuscado == null) {
			brUsuario.addError(new FieldError("usuario", "email", "O e-mail ou senha incorretos"));
		}

		// Verifica se h� erros no BindingResult
		if (brUsuario.hasFieldErrors("email") || brUsuario.hasFieldErrors("senha")) {
			System.out.println("Deu erro");
			System.out.println(brUsuario);
			return "index";
		}
		

		// Aplicando o usu�rio na sess�o
		System.out.println("usuario " + usuarioBuscado);
		session.setAttribute("usuarioAutenticado", usuarioBuscado);
		
		if(usuarioBuscado.getTipo() == TiposUsuario.ADMINISTRADOR) {
			return "redirect:/app/adm";
		}else {

		return "redirect:/app/";
		}
	}
	
	@GetMapping({ "/sair" })
	public String logout() {

		return "redirect:/";
	}
	
}
