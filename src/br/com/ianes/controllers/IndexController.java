package br.com.ianes.controllers;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ianes.dao.AmbienteDAO;
import br.com.ianes.dao.ItemDAO;
import br.com.ianes.dao.MovimentacaoDAO;
import br.com.ianes.dao.PatrimonioDAO;
import br.com.ianes.dao.UsuarioDAO;

@Controller
@RequestMapping("/app")
public class IndexController {
	
	@Autowired
	private ServletContext context;
	@Autowired
	private UsuarioDAO usuarioDAO;
	@Autowired
	private AmbienteDAO ambienteDAO;
	@Autowired
	private PatrimonioDAO patrimonioDAO;
	@Autowired
	private ItemDAO itemDAO;
	@Autowired
	private MovimentacaoDAO movimentacaoDAO;
	
	@GetMapping(value = { "/", "" })
	public String abrirIndex(Model model) {
		System.out.println(context.getRealPath(""));

		return "usuario/indexUsuarioComum";
	}
	
	@GetMapping(value = { "/listaUsuarios"})
	public String abrirListaUsuários(Model model) {
		model.addAttribute("usuarios", usuarioDAO.buscarTodos());

		return "usuario/indexUsuarioComum";
	}
	
	@GetMapping(value = { "/listaPatrimonios"})
	public String abrirListaPatrimonios(Model model) {
		model.addAttribute("patrimonios", patrimonioDAO.buscarTodos());


		return "patrimonios/listaPatrimoniosComum";
	}
	
	@GetMapping(value = { "/listaAmbiente"})
	public String abrirListaAmbiente(Model model) {
		model.addAttribute("ambientes", ambienteDAO.buscarTodos());

		return "ambiente/listaAmbientesComum";
	}
	
	@GetMapping(value = { "/listaItensPatrimonio"})
	public String abrirListaItensPatrimonio(Model model) {
		model.addAttribute("itens", itemDAO.buscarTodos());

		return "itensPatrimonio/indexItensPatrimonio";
	}
	

	@GetMapping(value = { "/listaMovimentacoes"})
	public String abrirListaMovimentacoes(Model model, Long id) {
		model.addAttribute("movimentacoes", movimentacaoDAO.buscarPorIdDoItem(id));

		return "movimentacao/listaMovimentacoesComum";
	}

}
