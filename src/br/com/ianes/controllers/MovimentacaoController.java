package br.com.ianes.controllers;

import java.util.Date;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import br.com.ianes.dao.AmbienteDAO;
import br.com.ianes.dao.ItemDAO;
import br.com.ianes.dao.MovimentacaoDAO;
import br.com.ianes.models.ItemPatrimonio;
import br.com.ianes.models.Movimentacao;
import br.com.ianes.models.Usuario;

@Controller
public class MovimentacaoController {

	@Autowired
	ItemDAO itemDAO;

	@Autowired
	AmbienteDAO ambienteDAO;

	@Autowired
	MovimentacaoDAO movimentacaoDAO;

	@GetMapping("/app/adm/movimentacao/novo")
	public String abrirFormNovoUsuario(Model model, Long id, Date dtMovimentacao) {

		// Passando o objeto que ser� enviado pra tela
		model.addAttribute("ambientesPesquisa", ambienteDAO.buscarTodos());
		model.addAttribute("itemPatrimonio", itemDAO.buscar(id));
		// System.out.println("Alguma coisa antes: " +
		// movimentacaoDAO.buscarPorItem(id));
		model.addAttribute("ultimaMovimentacao", movimentacaoDAO.buscarPorItem(id));
		model.addAttribute("movimentacao", new Movimentacao());

		return "movimentacao/adm/form";
	}

	@PostMapping("/app/adm/movimentacao/salvar")
	public String salvarMovimentacao(@Valid Movimentacao movimentacao, BindingResult brMovimentacao, Model model,
			HttpSession session) {

		if (brMovimentacao.hasErrors()) {
			System.out.println(brMovimentacao);

			return "movimentacao/form";
		}

		if (movimentacao.getId() == null) {
			System.out.println("ENTROU NO IF");
			Usuario autenticado = (Usuario) session.getAttribute("usuarioAutenticado");
			System.out.println("USER DA SESSION");

			movimentacao.setUsuario(autenticado);
			System.out.println("PEGA O USER " + movimentacao.getUsuario().getNome());

			movimentacao.setDtMovimentacao(new Date(System.currentTimeMillis()));
			System.out.println("PEGA A DATA " + movimentacao.getDtMovimentacao());
			// AT� AQUI OK *******************************************************

			movimentacao.setItemPatrimonio(movimentacao.getItemPatrimonio());
			System.out.println("PEGA O ITEM " + movimentacao.getItemPatrimonio().getId());

			movimentacao.setAmbienteOrigem(movimentacao.getAmbienteOrigem());
			System.out.println("PEGA A ORIGEM " + movimentacao.getAmbienteOrigem().getNome());

			ItemPatrimonio itensAMovimentar = itemDAO.buscar(movimentacao.getItemPatrimonio().getId());
			System.out.println("BUSCA O ITEM A SER MOVIMENTADO");

			itensAMovimentar.setAmbiente(ambienteDAO.buscar(movimentacao.getAmbienteDestino().getId()));
			System.out.println("PEGA O AMBIENTE DE DESTINO");

			movimentacaoDAO.persistir(movimentacao);
			System.out.println("PERSISTIR");

			// ALTERANDO O LUGAR ONDE O ITEM ESTA
			itemDAO.alterar(itensAMovimentar);
			// System.out.println("ALTERAR O ITEM");
		}
		model.addAttribute("movimentacao", movimentacaoDAO.buscarTodos());
		// System.out.println("LISTA AS MOVIMENTA��ES");
		return "redirect:/app/adm/listaItensPatrimonio";

	}

	// USU�RIO NORMAL
	@GetMapping("/app/movimentacao/novo")
	public String abrirFormNovoUsuarioComum(Model model, Long id) {

		// Passando o objeto que ser� enviado pra tela
		model.addAttribute("ambientesPesquisa", ambienteDAO.buscarTodos());
		model.addAttribute("itemPatrimonio", itemDAO.buscar(id));
		model.addAttribute("ultimaMovimentacao", movimentacaoDAO.buscarPorItem(id));
		model.addAttribute("movimentacao", new Movimentacao());

		return "movimentacao/form";
	}

	@PostMapping("/app/movimentacao/salvar")
	public String salvarMovimentacaoUsuarioComum(@Valid Movimentacao movimentacao, BindingResult brMovimentacao,
			Model model, HttpSession session) {

		if (brMovimentacao.hasErrors()) {
			System.out.println(brMovimentacao);

			return "movimentacao/form";
		}

		if (movimentacao.getId() == null) {
			System.out.println("ENTROU NO IF");
			Usuario autenticado = (Usuario) session.getAttribute("usuarioAutenticado");
			System.out.println("USER DA SESSION");

			movimentacao.setUsuario(autenticado);
			System.out.println("PEGA O USER " + movimentacao.getUsuario().getNome());

			movimentacao.setDtMovimentacao(new Date(System.currentTimeMillis()));
			System.out.println("PEGA A DATA " + movimentacao.getDtMovimentacao());
			// AT� AQUI OK *******************************************************

			movimentacao.setItemPatrimonio(movimentacao.getItemPatrimonio());
			System.out.println("PEGA O ITEM " + movimentacao.getItemPatrimonio().getId());

			movimentacao.setAmbienteOrigem(movimentacao.getAmbienteOrigem());
			System.out.println("PEGA A ORIGEM " + movimentacao.getAmbienteOrigem().getNome());

			ItemPatrimonio itensAMovimentar = itemDAO.buscar(movimentacao.getItemPatrimonio().getId());
			System.out.println("BUSCA O ITEM A SER MOVIMENTADO");

			itensAMovimentar.setAmbiente(ambienteDAO.buscar(movimentacao.getAmbienteDestino().getId()));
			System.out.println("PEGA O AMBIENTE DE DESTINO");

			movimentacaoDAO.persistir(movimentacao);
			System.out.println("PERSISTIR");

			// ALTERANDO O LUGAR ONDE O ITEM ESTA
			itemDAO.alterar(itensAMovimentar);
			// System.out.println("ALTERAR O ITEM");

		}
		model.addAttribute("movimentacao", movimentacaoDAO.buscarTodos());
		// System.out.println("LISTA AS MOVIMENTA��ES");
		return "redirect:/app/listaItensPatrimonio";

	}
}
