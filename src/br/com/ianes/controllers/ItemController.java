package br.com.ianes.controllers;

import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ianes.dao.AmbienteDAO;
import br.com.ianes.dao.ItemDAO;
import br.com.ianes.dao.PatrimonioDAO;
import br.com.ianes.models.ItemPatrimonio;
import br.com.ianes.models.Usuario;

@Controller
public class ItemController {

	@Autowired
	private ItemDAO itemDAO;

	@Autowired
	private AmbienteDAO ambienteDAO;

	@Autowired
	private PatrimonioDAO patrimonioDAO;

	@GetMapping("/app/adm/item/novo")
	public String abrirFormNovoItem(Model model) {

		// Passando o objeto que ser� enviado pra tela
		model.addAttribute("itemPatrimonio", new ItemPatrimonio());
		model.addAttribute("pesquisasPatrimonio", patrimonioDAO.buscarTodos());
		model.addAttribute("pesquisasAmbientes", ambienteDAO.buscarTodos());

		return "itensPatrimonio/adm/form";
	}

	@GetMapping("/app/adm/item/editar")
	public String abrirEdicao(Model model, @RequestParam(name = "id", required = true) Long id,
			HttpServletResponse response) throws IOException {

		model.addAttribute("itemPatrimonio", itemDAO.buscar(id));

		return "itensPatrimonio/adm/form";
	}

	@GetMapping("/app/adm/item/deletar")
	public String deletar(@RequestParam(required = true) Long id, HttpServletResponse response) throws IOException {

		ItemPatrimonio itemPatrimonioADeletar = itemDAO.buscar(id);
		itemDAO.deletar(itemPatrimonioADeletar);

		return "redirect:/app/adm/listaItensPatrimonio";
	}

	@PostMapping("/app/adm/item/salvar")
	public String salvar(@Valid ItemPatrimonio itemPatrimonio, HttpSession session, BindingResult brItemPatrimonio,
			Model model) {

		System.out.println("iteeeeeemmm     :     " + itemPatrimonio);
		if (itemPatrimonio.getId() == null) {

			Usuario usuarioAutenticado = (Usuario) session.getAttribute("usuarioAutenticado");
			itemPatrimonio.setUsuario(usuarioAutenticado);

			itemDAO.persistir(itemPatrimonio);

		} else {
			if (brItemPatrimonio.hasErrors()) {
				return "itensPatrimonio/adm/form";
			}
		}
		return "redirect:/app/adm/listaItensPatrimonio";
	}
	
	//***************** Usuario Normal *****************
	@GetMapping("/app/item/novo")
	public String abrirFormNovoItemUsuarioNormal(Model model) {

		// Passando o objeto que ser� enviado pra tela
		model.addAttribute("itemPatrimonio", new ItemPatrimonio());
		model.addAttribute("pesquisasPatrimonio", patrimonioDAO.buscarTodos());
		model.addAttribute("pesquisasAmbientes", ambienteDAO.buscarTodos());

		return "itensPatrimonio/form";
	}
	
	@PostMapping("/app/item/salvar")
	public String salvarUsuarioNormal(@Valid ItemPatrimonio itemPatrimonio, HttpSession session, BindingResult brItemPatrimonio,
			Model model) {

		System.out.println("iteeeeeemmm     :     " + itemPatrimonio);
		if (itemPatrimonio.getId() == null) {

			Usuario usuarioAutenticado = (Usuario) session.getAttribute("usuarioAutenticado");
			itemPatrimonio.setUsuario(usuarioAutenticado);

			itemDAO.persistir(itemPatrimonio);

		} else {
			if (brItemPatrimonio.hasErrors()) {
				return "itensPatrimonio/form";
			}
		}
		return "redirect:/app/listaItensPatrimonio";
	}

}
