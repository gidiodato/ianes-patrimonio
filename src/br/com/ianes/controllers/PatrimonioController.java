package br.com.ianes.controllers;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ianes.dao.PatrimonioDAO;
import br.com.ianes.models.Patrimonio;
import br.com.ianes.models.Usuario;

@Controller
public class PatrimonioController {

	@Autowired
	private PatrimonioDAO patrimonioDAO;

	@GetMapping("/app/adm/patrimonio/novo")
	public String abrirFormNovoUsuario(Model model) {

		// Passando o objeto que ser� enviado pra tela
		model.addAttribute("patrimonio", new Patrimonio());

		return "patrimonios/adm/form";
	}

	@GetMapping("/app/adm/patrimonio/editar")
	public String abrirEdicao(Model model, @RequestParam(name = "id", required = true) Long id,
			HttpServletResponse response) throws IOException {

		model.addAttribute("patrimonio", patrimonioDAO.buscar(id));

		return "patrimonios/adm/form";
	}

	@GetMapping("/app/adm/patrimonio/deletar")
	public String deletar(@RequestParam(required = true) Long id, HttpServletResponse response) throws IOException {

		Patrimonio patrimonioADeletar = patrimonioDAO.buscar(id);
		patrimonioDAO.deletar(patrimonioADeletar);

		return "redirect:/app/adm/listaPatrimonios";
	}

	@PostMapping(value = { "/app/adm/patrimonio/salvar" })
	public String salvar(@Valid Patrimonio patrimonio, HttpSession session, BindingResult brPatrimonio
			) throws IOException {
		
		//System.out.println("PATRIMONIO CADASTRADOOOOO:::::::::: " + patrimonio);
			
		if (patrimonio.getId() == null) {
			patrimonio.setDtCadastro(new Date(System.currentTimeMillis()));
			
			Usuario usuarioAutenticado = (Usuario) session.getAttribute("usuarioAutenticado");
			patrimonio.setUsuario(usuarioAutenticado);

			if (patrimonioDAO.buscarPorNome(patrimonio.getNome()) != null) {
				brPatrimonio.addError(new FieldError("patrimonio", "nome", "O nome j� esta sendo usado"));
			}

			// Se houverem erros no usu�rio, reabre o formul�rio
			if (brPatrimonio.hasErrors()) {
				return "patrimonio/adm/form";
			}
		} else {
			// Valida��es de altera��o
			if (brPatrimonio.hasFieldErrors("nome") || brPatrimonio.hasFieldErrors("categoria")) {
				return "patrimonio/adm/form";
			}
		}

		// Caso cadastro
		if (patrimonio.getId() == null) {
			patrimonioDAO.persistir(patrimonio);
		}
		// Caso altera��o
		else

		{
			// Pega o usu�rio do Hibernate atrav�s do id do form para poder alter�-lo
			Patrimonio patrimonioBancoDeDados = patrimonioDAO.buscar(patrimonio.getId());
			patrimonioBancoDeDados.setNome(patrimonio.getNome());
			patrimonioBancoDeDados.setCategoria(patrimonio.getCategoria());

			patrimonioDAO.alterar(patrimonioBancoDeDados);
		}

		return "redirect:/app/adm/listaPatrimonios";
	}

}
